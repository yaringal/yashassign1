package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void testBoxComplete() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 4, 2);
        grid.drawHorizontal(0, 0, 1);
        grid.drawHorizontal(0, 1, 1);
        grid.drawVertical(0, 0, 1);
        grid.drawVertical(1, 0, 1);

        assertTrue(grid.boxComplete(0, 0), "Box at (0, 0) should be complete");
    }

    @Test
    public void testDrawHorizontalAlreadyDrawn() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 4, 2);
        grid.drawHorizontal(0, 0, 1);
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawHorizontal(0, 0, 1);
        });
        String expectedMessage = "Line already drawn";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testDrawVerticalAlreadyDrawn() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 4, 2);
        grid.drawVertical(0, 0, 1);
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> {
            grid.drawVertical(0, 0, 1);
        });
        String expectedMessage = "Line already drawn";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
